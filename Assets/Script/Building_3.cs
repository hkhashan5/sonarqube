﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SonarServise;

public class Building_3 : MonoBehaviour
{
    public TextMesh bugs;
    public TextMesh vulnerabilities;
    public TextMesh coverage;
    public TextMesh code_Smells;
    public TextMesh duplication;

    // Start is called before the first frame update
    void Start()
    {
        Service1 cds = new Service1();
        RootObject root = cds.CallServer();

        bugs.text = root.B_3.bugs;
        vulnerabilities.text = root.B_3.vulnerabilities;
        coverage.text = root.B_3.coverage;
        code_Smells.text = root.B_3.code_Smells;
        duplication.text = root.B_3.duplication;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
