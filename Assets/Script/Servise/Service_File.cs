﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarServise : MonoBehaviour
{
    [System.Serializable]
    public class B_1
    {
        public string bugs;
        public string vulnerabilities;
        public string coverage;
        public string code_Smells;
        public string duplication;
        public int lines_of_codes;
        public int violations;

    }

    [System.Serializable]
    public class B_2
    {
        public string bugs;
        public string vulnerabilities;
        public string coverage;
        public string code_Smells;
        public string duplication;
        public int lines_of_codes;
        public int violations;
    }

    [System.Serializable]
    public class B_3
    {
        public string bugs;
        public string vulnerabilities;
        public string coverage;
        public string code_Smells;
        public string duplication;
        public int lines_of_codes;
        public int violations;
    }



    [System.Serializable]
    public class RootObject
    {
        public B_1 B_1;
        public B_2 B_2;
        public B_3 B_3;

    }
}
