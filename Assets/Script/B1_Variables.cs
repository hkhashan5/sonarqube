﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SonarServise;


public class B1_Variables : MonoBehaviour
{
    public int violations;
    GameObject cube;
    public int y;
    // Start is called before the first frame update
    void Start()
    {
        Service1 cds = new Service1();
        RootObject root = cds.CallServer();

        violations = root.B_1.violations;
        y = root.B_1.lines_of_codes;

        transform.localScale = new Vector3(5, y, 5);

        if (violations <= 10)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;
        }
        else if (violations <= 20)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (violations <= 30)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
