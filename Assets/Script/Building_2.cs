﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SonarServise;

public class Building_2 : MonoBehaviour
{
    public TextMesh bugs;
    public TextMesh vulnerabilities;
    public TextMesh coverage;
    public TextMesh code_Smells;
    public TextMesh duplication;

    // Start is called before the first frame update
    void Start()
    {
        Service1 cds = new Service1();
        RootObject root = cds.CallServer();

        bugs.text = root.B_2.bugs;
        vulnerabilities.text = root.B_2.vulnerabilities;
        coverage.text = root.B_2.coverage;
        code_Smells.text = root.B_2.code_Smells;
        duplication.text = root.B_2.duplication;

    }

    // Update is called once per frame
    void Update()
    {

    }
}
