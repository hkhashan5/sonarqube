﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static SonarServise;


public class B3_Variables : MonoBehaviour
{
    GameObject cube;
    public int y;
    public int violations;

    // Start is called before the first frame update
    void Start()
    {
        //transform.localScale.y = y;  
        Service1 cds = new Service1();
        RootObject root = cds.CallServer();

        y = root.B_3.lines_of_codes;
        violations = root.B_3.violations;


        transform.localScale = new Vector3(5, y, 5);

        if (violations <= 10)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.green;
        }
        else if (violations <= 20)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.yellow;
        }
        else if (violations <= 30)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }

    }

    // Update is called once per frame
    void Update()
    {
    }
}
